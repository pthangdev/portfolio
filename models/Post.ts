export interface Post {
  id: string | number;
  title: string;
  publisedDate: string;
  tagList: string[];
  description: string;
}
