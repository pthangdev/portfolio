import { FC } from 'react';
import { LayoutProps } from './types';

const AuthLayout: FC<LayoutProps> = ({ children }) => {
  return <div>{children}</div>;
};

export default AuthLayout;
