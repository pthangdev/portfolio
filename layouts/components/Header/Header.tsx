import { AppBar, Container, Stack } from '@mui/material';
import Link from 'next/link';
import { Link as MuiLink } from '@mui/material';
import { FC } from 'react';

import routes from './routes';
import { useRouter } from 'next/router';

interface Props {}

const Header: FC<Props> = (props) => {
  const router = useRouter();
  return (
    <AppBar color="default" position="fixed" sx={{ height: '60px' }}>
      <Container sx={{ height: '100%' }}>
        <Stack justifyContent="flex-end" height="100%" spacing={4}>
          {routes.map((route) => {
            const isActiveLink = router.pathname === route.to;

            return (
              <Link key={route.to} href={route.to} passHref>
                <MuiLink underline="none" className={isActiveLink ? 'active' : ''} sx={{ py: 2 }}>
                  {route.label}
                </MuiLink>
              </Link>
            );
          })}
        </Stack>
      </Container>
    </AppBar>
  );
};

export default Header;
