const routes = [
  {
    label: 'Home',
    to: '/',
  },
  {
    label: 'Works',
    to: '/works',
  },
  {
    label: 'Blog',
    to: '/blog',
  },
];

export default routes;
