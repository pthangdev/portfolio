import { Box, Stack, Typography } from '@mui/material';
import { FC } from 'react';
import socialLinks from './socialLinks';

interface Props {}

const Footer: FC<Props> = (props) => {
  return (
    <Box component="footer" py="52px" textAlign="center">
      <Stack spacing="35px" mb="26px" justifyContent="center">
        {socialLinks.map((social) => {
          const { url, icon: Icon } = social;
          return (
            <Box
              key={url}
              component="a"
              href={url}
              target="_blank"
              rel="noopener norefferer"
              sx={{
                '&:hover': {
                  '.social-icon': {
                    color: 'primary.main',
                    transform: 'scale(1.1)',
                  },
                },
              }}
            >
              {<Icon className="social-icon" sx={{ transition: 'transform 0.4s' }} />}
            </Box>
          );
        })}
      </Stack>
      <Typography textAlign="center">Copyright ©2020 All rights reserved </Typography>
    </Box>
  );
};

export default Footer;
