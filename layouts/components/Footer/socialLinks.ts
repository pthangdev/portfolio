import { FacebookIcon, InstagramIcon, LinkedInIcon, TwitterIcon } from '~/components/Icons';

const socialLinks = [
  {
    icon: FacebookIcon,
    url: 'https://www.facebook.com/',
  },
  {
    icon: InstagramIcon,
    url: 'https://www.instagram.com/',
  },
  {
    icon: TwitterIcon,
    url: 'https://www.twitter.com/',
  },
  {
    icon: LinkedInIcon,
    url: 'https://www.linkedin.com/',
  },
];

export default socialLinks;
