import { FC } from 'react';
import { LayoutProps } from './types';

const EmptyLayout: FC<LayoutProps> = ({ children }) => {
  return <>{children}</>;
};

export default EmptyLayout;
