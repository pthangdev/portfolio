import { Box, Stack } from '@mui/material';
import { FC, ReactNode } from 'react';

import Footer from './components/Footer/Footer';
import Header from './components/Header';
import { LayoutProps } from './types';

const DefaultLayout: FC<LayoutProps> = ({ children }) => {
  return (
    <Box>
      <Header />
      <Stack direction="column" minHeight="100vh">
        <Box component="main" sx={{ flex: 1 }} pt="60px">
          {children}
        </Box>
        <Footer />
      </Stack>
    </Box>
  );
};

export default DefaultLayout;
