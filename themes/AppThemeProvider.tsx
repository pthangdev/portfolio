import { CssBaseline, ThemeProvider } from '@mui/material';
import { FC, ReactNode } from 'react';
import theme from './theme';

interface Props {
  children: ReactNode;
}

const AppThemeProvider: FC<Props> = ({ children }) => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};

export default AppThemeProvider;
