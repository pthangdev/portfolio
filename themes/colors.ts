const colors = {
  primary: '#FF6464',
  secondary: '#00A8CC',

  black: '#21243D',
  white: '#fff',

  blueDark: '#142850',

  light: '#EDF7FA',
};
export default colors;
