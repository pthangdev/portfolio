import { createTheme, experimental_sx as sx, responsiveFontSizes } from '@mui/material';
import { red } from '@mui/material/colors';
import colors from './colors';

// Create a theme instance.
let theme = createTheme({
  palette: {
    mode: 'light',
    primary: {
      main: colors.primary,
    },
    secondary: {
      main: colors.secondary,
      light: colors.light,
    },
    error: {
      main: red.A400,
    },
    text: {
      primary: colors.black,
    },
  },
  typography: {
    fontFamily: ['Heebo, san-serif'].join(','),
    allVariants: {
      color: '#21243D',
    },
  },

  components: {
    MuiLink: {
      defaultProps: {
        underline: 'hover',
      },
      styleOverrides: {
        root: {
          color: colors.black,
          fontWeight: 500,
          '&:hover, &.active': {
            color: colors.primary,
            textDecoration: 'underline',
          },
        },
      },
    },

    MuiStack: {
      defaultProps: {
        direction: 'row',
        alignItems: 'center',
      },
    },

    MuiContainer: {
      defaultProps: {
        maxWidth: 'md',
      },
      styleOverrides: {
        maxWidthSm: {
          maxWidth: '680px',
          '@media (min-width: 600px)': {
            maxWidth: '680px',
          },
        },
        maxWidthMd: {
          '@media (min-width: 600px)': {
            paddingLeft: '20px',
            paddingRight: '20px',
          },
        },
      },
    },

    MuiButton: {
      variants: [
        {
          props: { variant: 'contained', color: 'primary' },
          style: {
            color: colors.white,
          },
        },
      ],
    },

    MuiChip: {
      styleOverrides: {
        root: {
          paddingInline: 4,
          paddingTop: 4,
          paddingBottom: 4,
        },
      },
      variants: [
        {
          props: {
            color: 'secondary',
          },
          style: {
            fontWeight: 'bold',
            color: colors.white,
            fontSize: 16,
            backgroundColor: colors.blueDark,
          },
        },
      ],
    },
  },
});

theme = responsiveFontSizes(theme);
export default theme;
