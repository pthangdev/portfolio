import type { NextPage } from 'next';
import Head from 'next/head';
import { ReactElement } from 'react';
import DefaultLayout from '~/layouts/DefaultLayout';
import { NextPageWithLayout } from '~/layouts/types';

export interface WorkPageProps {}

const WorkPage: NextPageWithLayout<WorkPageProps> = (props) => {
  return (
    <>
      {/* Head */}
      <Head>
        <title>WorkPage</title>
      </Head>
      {/* Body */}
      <div>WorkPage</div>
    </>
  );
};
WorkPage.getLayout = (page: ReactElement) => {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default WorkPage;
