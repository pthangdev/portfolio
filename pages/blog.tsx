import type { NextPage } from 'next';
import Head from 'next/head';
import { ReactElement } from 'react';

import DefaultLayout from '~/layouts/DefaultLayout';
import { NextPageWithLayout } from '~/layouts/types';

export interface BlogPageProps {}

const BlogPage: NextPageWithLayout = (props) => {
  return (
    <>
      {/* Head */}
      <Head>
        <title>BlogPage</title>
      </Head>
      {/* Body */}
      <div>BlogPage</div>
    </>
  );
};
BlogPage.getLayout = (page: ReactElement) => {
  return <DefaultLayout>{page}</DefaultLayout>;
};
export default BlogPage;
