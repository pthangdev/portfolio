import { Box } from '@mui/material';
import Head from 'next/head';
import { ReactElement } from 'react';

import { FeatureWorks, HeroSection, RecentPosts } from '~/components/home';
import DefaultLayout from '~/layouts/DefaultLayout';
import { NextPageWithLayout } from '~/layouts/types';

export interface HomePageProps {}

const HomePage: NextPageWithLayout = (props) => {
  return (
    <>
      {/* Head */}
      <Head>
        <title>HomePage</title>
      </Head>
      {/* Body */}
      <Box>
        <HeroSection />
        <RecentPosts />
        <FeatureWorks />
      </Box>
    </>
  );
};
HomePage.getLayout = (page: ReactElement) => {
  return <DefaultLayout>{page}</DefaultLayout>;
};
export default HomePage;
