import { CacheProvider, EmotionCache } from '@emotion/react';
import { CssBaseline, ThemeProvider } from '@mui/material';
import type { AppProps } from 'next/app';
import Head from 'next/head';
import '../styles/globals.css';

import createEmotionCache from '~/themes/createEmotionCache';
import theme from '~/themes/theme';
import EmptyLayout from '~/layouts/EmptyLayout';
import { AppPropsWithLayout } from '~/layouts/types';
import AppThemeProvider from '~/themes/AppThemeProvider';

interface MyAppProps extends AppPropsWithLayout {
  emotionCache?: EmotionCache;
}

const clientSideEmotionCache = createEmotionCache();

export default function App(props: MyAppProps) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;
  // Use the layout defined at the page level, if available
  const getLayout = Component.getLayout ?? ((page) => page);
  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>
      <AppThemeProvider>{getLayout(<Component {...pageProps} />)}</AppThemeProvider>
    </CacheProvider>
  );
}
