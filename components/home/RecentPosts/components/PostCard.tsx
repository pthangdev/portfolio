import { Card, CardContent, Divider, Typography } from '@mui/material';
import { FC } from 'react';
import { Post } from '~/models';

interface Props {
  post: Post;
}

const PostCard: FC<Props> = ({ post }) => {
  return (
    <Card>
      <CardContent>
        <Typography variant="h5" fontWeight="bold">
          {post.title}
        </Typography>
        <Typography variant="body1" component="div" my={2} sx={{ display: 'flex' }}>
          {post.publisedDate}
          <Divider orientation="vertical" sx={{ mx: 2 }} flexItem />
          {post.tagList.join(', ')}
        </Typography>
        <Typography variant="body2">{post.description}</Typography>
      </CardContent>
    </Card>
  );
};

export default PostCard;
