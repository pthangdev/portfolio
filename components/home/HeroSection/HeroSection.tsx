import { Box, Button, Container, Stack, Typography } from '@mui/material';
import Image from 'next/image';
import { FC } from 'react';

import avatarImg from '~/assets/images/home/avatar.png';

interface Props {}

const HeroSection: FC<Props> = (props) => {
  return (
    <Box component="section">
      <Container>
        <Stack
          direction={{ xs: 'column-reverse', md: 'row' }}
          alignItems={{ xs: 'center', md: 'flex-start' }}
          spacing={{ xs: '34px', md: '115px' }}
          pt={{ xs: '32px', md: '160px' }}
          pb={{ xs: '58px', md: '70px' }}
          textAlign={{ xs: 'center', md: 'start' }}
        >
          {/* Left */}
          <Box sx={{ flex: 2 }}>
            <Typography
              variant="h3"
              component="h1"
              fontWeight="bold"
              mb={{ xs: '21px', md: '40px' }}
            >
              Hi, I am John,
              <br /> Creative Technologist
            </Typography>
            <Typography mb={{ xs: '27px', md: '38px' }}>
              Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit
              officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud
              amet.
            </Typography>
            <Button variant="contained" size="large" sx={{ mx: 'auto' }}>
              Download Resume
            </Button>
          </Box>
          {/* Right */}
          <Box
            sx={{
              flex: 1,
              minWidth: '240px',
              boxShadow: '-5px 13px',
              color: 'secondary.light',
              borderRadius: '50%',
            }}
          >
            <Image src={avatarImg} layout="responsive" alt="avatar" />
          </Box>
        </Stack>
      </Container>
    </Box>
  );
};

export default HeroSection;
