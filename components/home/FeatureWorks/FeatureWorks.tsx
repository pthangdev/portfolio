import { Box, Container, Typography } from '@mui/material';
import { FC } from 'react';

import { WorkList } from '~/components/work';
import { Work } from '~/models';
import workImg from '~/assets/images/work.jpg';

interface Props {}

const FeatureWorks: FC<Props> = (props) => {
  const workList: Work[] = [
    {
      id: '1',
      title: 'Designing Dashboards',
      shortDescription:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
      fullDescription:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
      tagList: ['Express', 'Handlebars'],
      thumbnailUrl: workImg.src,
      createdAt: '2020',
      updatedAt: '2021',
    },
    {
      id: '2',
      title: 'Vibrant Portraits of 2020',
      shortDescription:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
      fullDescription:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
      tagList: ['Illustration'],
      thumbnailUrl: workImg.src,
      createdAt: '2018',
      updatedAt: '2021',
    },
    {
      id: '3',
      title: '36 Days of Malayalam type',
      shortDescription:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
      fullDescription:
        'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
      tagList: ['Typography'],
      thumbnailUrl: workImg.src,
      createdAt: '2018',
      updatedAt: '2021',
    },
  ];

  return (
    <Box component="section">
      <Container>
        <Typography variant="h5" my="30px">
          Featured works
        </Typography>
        <WorkList workList={workList} />
      </Container>
    </Box>
  );
};

export default FeatureWorks;
