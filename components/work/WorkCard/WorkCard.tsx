import { Box, Chip, Stack, Typography } from '@mui/material';
import Image from 'next/image';
import { FC } from 'react';
import { Work } from '~/models';

interface Props {
  work: Work;
}

const WorkCard: FC<Props> = ({ work }) => {
  return (
    <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2}>
      <Box width={{ xs: '100%', md: '246px' }} flexShrink={0}>
        <Image
          src={work.thumbnailUrl}
          width={246}
          height={180}
          layout="responsive"
          alt={work.thumbnailUrl}
        />
      </Box>
      <Box>
        <Typography variant="h4" fontWeight="bold">
          {work.title}
        </Typography>

        <Stack my={2}>
          <Chip color="secondary" label={work.createdAt} size="small" />
          <Typography ml={3}>{work.tagList.join(', ')}</Typography>
        </Stack>

        <Typography color="GrayText">{work.shortDescription}</Typography>
      </Box>
    </Stack>
  );
};

export default WorkCard;
