import { Box, Divider } from '@mui/material';
import { FC } from 'react';
import { Work } from '~/models';
import WorkCard from '../WorkCard';

interface Props {
  workList: Work[];
}

const WorkList: FC<Props> = ({ workList }) => {
  if (workList.length === 0) return null;

  return (
    <Box>
      {workList.map((work) => (
        <Box key={work.id}>
          <WorkCard work={work} />
          <Divider sx={{ my: 3 }} />
        </Box>
      ))}
    </Box>
  );
};

export default WorkList;
